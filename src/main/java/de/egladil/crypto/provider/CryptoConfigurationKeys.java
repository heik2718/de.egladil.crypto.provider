//=====================================================
// Projekt: de.egladil.crypto.provider
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.crypto.provider;

/**
 * CryptoConfigurationKeys
 */
public interface CryptoConfigurationKeys {

	String CRYPTO_PRIVATE_SALT = "crypto.private_salt";

	String CRYPTO_ALGORITHM_NAME = "crypto.algorithm";

	String CRYPTO_ITERATIONS = "crypto.iterations";

	String PW_LENGTH = "pw.length";

	String PW_CHARPOOL = "pw.charpool";

	String PW_RANDOM_ALGORITHM = "pw.random_algorithm";



	String LOGIN_BOT_DELAY = "login.bot_delay";

	String LOGIN_ACCESS_TOKEN_EXPIRE_TIME = "login.accesstoken_expire_time";

	String LOGIN_ACCESS_TOKEN_CACHE_SIZE = "login.accesstoken_cache_size";

	String REGISTRATION_KEY_EXPIRE_PERIOD = "registration.key_expire_period";

}
