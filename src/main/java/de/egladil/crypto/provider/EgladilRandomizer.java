//=====================================================
// Projekt: de.egladil.crypto.provider
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.crypto.provider;

import java.security.SecureRandom;

/**
 * EgladilRandomizer. Ist angelehnt an ESAPI.
 */
public class EgladilRandomizer {

	private final SecureRandom secureRandom;

	/**
	 * Erzeugt eine Instanz von EgladilRandomizer
	 */
	public EgladilRandomizer(SecureRandom secureRandom) {
		this.secureRandom = secureRandom;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getRandomString(int length, char[] characterSet) {
		StringBuilder sb = new StringBuilder();
		for (int loop = 0; loop < length; loop++) {
			int index = secureRandom.nextInt(characterSet.length);
			sb.append(characterSet[index]);
		}
		String nonce = sb.toString();
		return nonce;
	}
}
