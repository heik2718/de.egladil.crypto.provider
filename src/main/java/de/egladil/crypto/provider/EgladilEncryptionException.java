//=====================================================
// Projekt: de.egladil.exceptions
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.crypto.provider;

/**
 * @author heike
 *
 */
public class EgladilEncryptionException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public EgladilEncryptionException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public EgladilEncryptionException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public EgladilEncryptionException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EgladilEncryptionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
