//=====================================================
// Projekt: de.egladil.crypto.provider
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.crypto.provider.impl;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.UUID;

import javax.inject.Inject;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.Hash;
import org.apache.shiro.crypto.hash.HashRequest;
import org.apache.shiro.crypto.hash.HashService;
import org.apache.shiro.crypto.hash.SimpleHashRequest;
import org.apache.shiro.util.ByteSource;
import org.apache.shiro.util.SimpleByteSource;

import com.google.inject.Singleton;
import com.google.inject.name.Named;

import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.crypto.provider.CryptoConfigurationKeys;
import de.egladil.crypto.provider.EgladilEncryptionException;
import de.egladil.crypto.provider.EgladilRandomizer;
import de.egladil.crypto.provider.IEgladilCryptoUtils;

/**
 * Wrapper für Apache Shiro
 *
 * @author root
 *
 */
@Singleton
public class EgladilCryptoUtilsImpl implements IEgladilCryptoUtils {

	private final IEgladilConfiguration config;

	private final EgladilRandomizer egladilRandomizer;

	/**
	 * Erzeugt eine Instanz von EgladilCryptoUtilsImpl
	 */
	@Inject
	public EgladilCryptoUtilsImpl(@Named("configRoot") final String pathConfigRoot) {
		config = new EgladilCryptoConfiguration(pathConfigRoot);
		final String algorithm = config.getProperty(CryptoConfigurationKeys.PW_RANDOM_ALGORITHM);
		try {
			final SecureRandom secureRandom = SecureRandom.getInstance(algorithm);
			egladilRandomizer = new EgladilRandomizer(secureRandom);
		} catch (final NoSuchAlgorithmException e) {
			throw new EgladilEncryptionException("Error creating randomizer: Can't find random algorithm " + algorithm, e);
		}

	}

	/**
	 * Generiert ein Salz der gegebenen Länge.
	 *
	 * @param saltLengthBits int ist in Bits, d.h. Ergebnis ist 1/8 so lang.
	 * @return
	 */
	char[] generateSalt(final int saltLengthBits) {
		final int byteSize = saltLengthBits / 8; // generatedSaltSize is in *bits* - convert to byte size:
		return new SecureRandomNumberGenerator().nextBytes(byteSize).toBase64().toCharArray();
	}

	/**
	 * @see de.egladil.crypto.provider.IEgladilCryptoUtils#hashPassword(char[])
	 */
	@Override
	public Hash hashPassword(final char[] password) {
		final String cryptoAlgorithmName = config.getProperty(CryptoConfigurationKeys.CRYPTO_ALGORITHM_NAME);
		final Integer cryptoIterations = config.getIntegerProperty(CryptoConfigurationKeys.CRYPTO_ITERATIONS);
		return this.hashPassword(password, cryptoAlgorithmName, cryptoIterations);
	}

	/**
	 * Das gegebene Passwort wird gesalzen und gehashed. Das generierte Salz, der hash-Wert und alle Infos zum erneuten
	 * prüfen mit checkPassword() stehen im zurückgegenen Hash und müssen persistiert werden. Den Hashwert holt man mit
	 * getBytes(), das Salz mit getSalt(). <br>
	 * <br>
	 * Das Salz und der Hash-Wert werden in der DB am besten mit der toBase64()-Methode von ByteSource gespeichert.
	 * Rückkonvertierung dann mit java.util.Base64.
	 *
	 * @param password
	 * @return Hash
	 */
	private Hash hashPassword(final char[] password, final String algorithmName, final Integer iterations) {
		final ByteSource salt = new SimpleByteSource(this.generateSalt(128));
		return this.hashPassword(password, algorithmName, salt, iterations);
	}

	/**
	 * Das gegebene Passwort wird gesalzen und gehashed. Das Salz, der hash-Wert und alle Infos zum erneuten prüfen mit
	 * checkPassword() stehen im zurückgegenen Hash und müssen persistiert werden. Den Hashwert holt man mit getBytes(),
	 * das Salz mit getSalt(). <br>
	 * <br>
	 * Das Salz und der Hash-Wert werden in der DB am besten mit der toBase64()-Methode von ByteSource gespeichert.
	 * Rückkonvertierung dann mit java.util.Base64.
	 *
	 * @param password
	 * @return Hash
	 */
	private Hash hashPassword(final char[] password, final String algorithmName, final ByteSource salt, final Integer iterations) {
		final HashService hashService = getHashService();

		final HashRequest hashRequest = new SimpleHashRequest(algorithmName, new SimpleByteSource(password), salt, iterations);

		final Hash hash = hashService.computeHash(hashRequest);
		return hash;
	}

	/**
	 * @see de.egladil.crypto.provider.IEgladilCryptoUtils#isPasswordCorrect(char[], java.lang.String, java.lang.String,
	 * java.lang.String, int)
	 */
	@Override
	public boolean isPasswordCorrect(final char[] password, final String persistentHashValue, final String persistentSalt, final String algorithmName,
		final int numberIterations) {
		final ByteSource salt = new SimpleByteSource(Base64.getDecoder().decode(persistentSalt));

		final Hash expectedHash = hashPassword(password, algorithmName, salt, numberIterations);

		final String expectedHashValue = new SimpleByteSource(expectedHash.getBytes()).toBase64();
		if (MessageDigest.isEqual(expectedHashValue.getBytes(), persistentHashValue.getBytes())) {
			return true;
		}
		return false;
	}

	/**
	 * @see de.egladil.crypto.provider.IEgladilCryptoUtils#getHashService()
	 */
	@Override
	public DefaultHashService getHashService() {
		final DefaultHashService hashService = new DefaultHashService();
		hashService.setPrivateSalt(new SimpleByteSource(config.getProperty(CryptoConfigurationKeys.CRYPTO_PRIVATE_SALT)));
		return hashService;
	}

	/**
	 * @see de.egladil.crypto.provider.IEgladilCryptoUtils#generateRandomPasswort()
	 */
	@Override
	public String generateRandomPasswort() {
		return generateRandomString(config.getIntegerProperty(CryptoConfigurationKeys.PW_LENGTH),
			config.getProperty(CryptoConfigurationKeys.PW_CHARPOOL).toCharArray());
	}

	/**
	 * @see de.egladil.crypto.provider.IEgladilCryptoUtils#generateRandomString(int)
	 */
	@Override
	public String generateRandomString(final int length) {
		return generateRandomString(length, config.getProperty(CryptoConfigurationKeys.PW_CHARPOOL).toCharArray());
	}

	/**
	 * @see de.egladil.crypto.provider.IEgladilCryptoUtils#generateRandomString(int, char[])
	 */
	@Override
	public String generateRandomString(final int length, final char[] charPool) {
		try {
			return egladilRandomizer.getRandomString(length, charPool);
		} catch (final Exception e) {
			throw new EgladilEncryptionException("Fehler beim generieren eines Zufallsstrings: " + e.getMessage(), e);
		}
	}

	/**
	 * @see de.egladil.crypto.provider.IEgladilCryptoUtils#getConfig()
	 */
	@Override
	public IEgladilConfiguration getConfig() {
		return config;
	}

	/**
	 * @see de.egladil.crypto.provider.IEgladilCryptoUtils#generateShortUuid()
	 */
	@Override
	public String generateShortUuid() {
		final UUID uuid = UUID.randomUUID();
		final long msb = uuid.getMostSignificantBits();
		final byte[] uuidBytes = ByteBuffer.allocate(8).putLong(msb).array();
		final String encoded = Base64.getEncoder().encodeToString(uuidBytes).replaceAll("\\+", "").replaceAll("\\=", "").replaceAll("/",
			"");
		return encoded;
	}

	@Override
	public String createUserIdReference() {
		long msb = UUID.randomUUID().getMostSignificantBits();
		return Long.toHexString(msb);
	}
}
