//=====================================================
// Projekt: de.egladil.crypto.provider
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.crypto.provider.impl;

import de.egladil.common.config.AbstractEgladilConfiguration;

/**
 * EgladilCryptoConfiguration
 */
public class EgladilCryptoConfiguration extends AbstractEgladilConfiguration {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von EgladilCryptoConfiguration
	 */
	public EgladilCryptoConfiguration() {
		super();
	}

	/**
	 * Erzeugt eine Instanz von EgladilCryptoConfiguration
	 */
	public EgladilCryptoConfiguration(final String pathConfigRoot) {
		super(pathConfigRoot);
	}

	@Override
	protected String getConfigFileName() {
		return "heike";
	}
}
