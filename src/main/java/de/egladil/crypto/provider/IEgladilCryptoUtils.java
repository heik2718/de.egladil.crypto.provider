package de.egladil.crypto.provider;

import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.Hash;

import de.egladil.common.config.IEgladilConfiguration;

public interface IEgladilCryptoUtils {

	/**
	 * Das gegebene Passwort wird gesalzen und gehashed. Das generierte Salz, der hash-Wert und alle Infos zum erneuten
	 * prüfen mit checkPassword() stehen im zurückgegenen Hash und müssen persistiert werden. Den Hashwert holt man mit
	 * getBytes(), das Salz mit getSalt(). <br>
	 * <br>
	 * Das Salz und der Hash-Wert werden in der DB am besten mit der toBase64()-Methode von ByteSource gespeichert.
	 * Rückkonvertierung dann mit java.util.Base64.
	 *
	 * @param password
	 * @return Hash
	 */
	Hash hashPassword(char[] password);

	/**
	 * Prüft das gegebene Passwort gegen das persistierte Passwort.
	 *
	 * @param password String das Passwort aus dem Logindialog.
	 * @param persistentHashValue String der Base64-encodede Passworthash aus der DB.
	 * @param persistentSalt String das Base64-encodede Salt aus der DB
	 * @param algorithmName String der zum Berechnen des PasswortHashes verwendete Algorithmus.
	 * @param numberIterations int die zum Berechnen des Passworthashes verwendete Anzahl Iterationen.
	 * @return
	 */
	boolean isPasswordCorrect(char[] password, String persistentHashValue, String persistentSalt, String algorithmName,
		int numberIterations);

	/**
	 * Gibt einen HashService mit einem privaten Salz zurück.
	 *
	 * @return
	 */
	DefaultHashService getHashService();

	/**
	 * Verwendet die pw- Einstellungen aus der crypto-Konfiguration, um einen Zufallsstring zu generieren. Basiert auf
	 * SecureRandom.
	 *
	 * @return
	 */
	String generateRandomPasswort() throws EgladilEncryptionException;

	/**
	 * Generiert einen Zufallsstring der gegebenen Länge. Das Char-Pool stammt aus pw.charpool. Basiert auf
	 * SecureRandom.
	 *
	 * @param length
	 * @return
	 */
	String generateRandomString(int length) throws EgladilEncryptionException;

	/**
	 * Generiert einen Zufallsstring der gegeben Länge mit den Zeichen charPool. Basiert auf SecureRandom.
	 *
	 * @param length
	 * @param charPool
	 * @return String
	 */
	String generateRandomString(int length, char[] charPool) throws EgladilEncryptionException;

	/**
	 * Liefert die Membervariable config
	 *
	 * @return die Membervariable config
	 */
	IEgladilConfiguration getConfig();

	/**
	 * Base64-encoded die most significant bytes einer UUID. Das Ergebnis ist 10 oder 11 Zeichen lang und kann z.B. als
	 * Einmalpasswort verwendet werden.
	 *
	 * @return String.
	 */
	String generateShortUuid() throws EgladilEncryptionException;

	/**
	 * Erzeugt eine User-ID-Referenz, mit der der User für den Zeitraum der Session identifiziert werden kann.
	 *
	 * @return String
	 */
	String createUserIdReference();
}