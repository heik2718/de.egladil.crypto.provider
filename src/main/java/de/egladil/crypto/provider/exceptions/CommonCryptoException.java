//=====================================================
// Project: de.egladil.crypto.provider
// (c) Heike Winkelvoß
//=====================================================
package de.egladil.crypto.provider.exceptions;

/**
 * CommonCryptoException
 */
public class CommonCryptoException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public CommonCryptoException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public CommonCryptoException(String arg0) {
		super(arg0);
	}

}
